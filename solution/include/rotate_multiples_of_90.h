#ifndef ROTATE_MULTIPLES_OF_90_H
#define ROTATE_MULTIPLES_OF_90_H

#include "inner_format.h"
struct image rotate(const struct image* input_image, const int degrees);

#endif
