#ifndef BMP_CONVERTOR_H
#define BMP_CONVERTOR_H

#include "inner_format.h"
#include <stdbool.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum from_bmp_status {
    READ_OK = 0,
    READ_INVALID_FILE,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_INVALID_PADDING
};

enum from_bmp_status from_bmp(FILE* file, struct image* to_image);

enum to_bmp_status {
    WRITE_OK = 0,
    WRITE_FAILED_HEADER,
    WRITE_NO_PIXELS,
    WRITE_FAILED_PIXELS
};

enum to_bmp_status to_bmp(FILE* file, const struct image* from_image);

#endif
