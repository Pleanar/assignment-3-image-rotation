#ifndef INNER_FORMAT_H
#define INNER_FORMAT_H

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t blue, green, red;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(const uint64_t width, const uint64_t height);
void image_destroy(const struct image* image);

#endif
