#ifndef FILE_OPEN_CLOSE_H
#define FILE_OPEN_CLOSE_H

#include <stdbool.h>
#include <stdio.h>

bool file_open(const char* fleName, FILE** file, const char* mode);

bool file_close(FILE* file);

#endif
