#include "../include/file_open_close.h"
#include "../include/bmp_convertor.h"
#include "../include/rotate_multiples_of_90.h"
#include <stdio.h>
#include <stdlib.h>

#define WRONG_AMOUNT_OF_ARGUMENTS (-1)
#define FILE_OPEN_FAILED (-2)
#define FILE_CLOSE_FAILED (-3)
#define WRONG_AMOUNT_OF_DEGREES (-4)

#define AMOUNT_OF_ARGUMENTS 4

int main( int argc, char** argv ) {
    if (argc != AMOUNT_OF_ARGUMENTS){
        fprintf(stderr, "The program requires 3 arguments, given: %d", argc - 1);
        return WRONG_AMOUNT_OF_ARGUMENTS;
    }

    //Opening initial file... If not able to, then don't proceed.
    FILE* input_FILE = NULL;
    if (!file_open(argv[1], &input_FILE, "r")){
        fprintf(stderr, "Failed to open input file");
        return FILE_OPEN_FAILED;
    }
    else {
        struct image input_image = {0};

        switch (from_bmp(input_FILE, &input_image)) {
            case READ_OK:
                break;
            case READ_INVALID_FILE:
                fprintf(stderr, "No file was given to convert.");
                return READ_INVALID_FILE;
            case READ_INVALID_HEADER:
                fprintf(stderr, "Could not create a header.");
                return READ_INVALID_HEADER;
            case READ_INVALID_BITS:
                fprintf(stderr, "Something went wrong with reading pixels of the image.");
                return READ_INVALID_BITS;
            case READ_INVALID_PADDING:
                fprintf(stderr, "Something went wrong with skipping the padding.");
                return READ_INVALID_PADDING;
        }

        if (!file_close(input_FILE)){
            fprintf(stderr, "Failed to close input file.");
            return FILE_CLOSE_FAILED;
        }

        int rotation_degree = atoi(argv[3]);

        if (rotation_degree % 90 != 0){
            image_destroy(&input_image);
            fprintf(stderr, "Program only supports angle of rotation that is a multiple of 90.");
            return WRONG_AMOUNT_OF_DEGREES;
        }

        struct image output_image = rotate(&input_image, rotation_degree);
        image_destroy(&input_image);
        FILE* output_file = NULL;

        if(!file_open(argv[2], &output_file, "w")){
            image_destroy(&output_image);
            fprintf(stderr, "Failed to open output file.");
            return FILE_OPEN_FAILED;
        }

        switch (to_bmp(output_file, &output_image)) {
            case WRITE_OK:
                break;
            case WRITE_FAILED_HEADER:
                fprintf(stderr, "Failed to write header to a file.");
                image_destroy(&output_image);
                if(!file_close(output_file)){
                    fprintf(stderr, "Failed to close output file.");
                }
                return WRITE_FAILED_HEADER;
            case WRITE_NO_PIXELS:
                fprintf(stderr, "No pixels from image to write to a file.");
                image_destroy(&output_image);
                if(!file_close(output_file)){
                    fprintf(stderr, "Failed to close output file.");
                }
                return WRITE_NO_PIXELS;
            case WRITE_FAILED_PIXELS:
                fprintf(stderr, "Something went wrong with writing pixels to a file.");
                image_destroy(&output_image);
                if(!file_close(output_file)){
                    fprintf(stderr, "Failed to close output file.");
                }
                return WRITE_FAILED_PIXELS;
        }

        image_destroy(&output_image);
        if(!file_close(output_file)){
            fprintf(stderr, "Failed to close output file.");
            return FILE_CLOSE_FAILED;
        }
    }

    return 0;
}
