#include "../include/inner_format.h"
#include <stdlib.h>

struct image image_create(const uint64_t width, const uint64_t height){
    if (width == 0 || height == 0) return (struct image) {0};
    struct image image_to_return = (struct image) {.width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)};
    if (image_to_return.data == NULL) return (struct image) {0};
    return image_to_return;
}

void image_destroy(const struct image* image){
    free(image->data);
}
