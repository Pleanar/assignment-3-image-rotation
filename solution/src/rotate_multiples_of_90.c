#include "../include/rotate_multiples_of_90.h"

static void rotate_coordinates_by_neg90(uint32_t rotations, uint32_t width, uint32_t height, uint32_t* x, uint32_t* y){
    while(rotations > 0){
        uint32_t temp = *y;
        *y = *x;
        *x = height - 1 - temp;
        rotations = rotations - 1;
        temp = height;
        height = width;
        width = temp;
    }
}

struct image rotate(const struct image* input_image, const int degrees){
    if (input_image->width == 0 || input_image->height == 0 || input_image->data == NULL) return (struct image) {0};
    //Something wrong with coordinates rotation: rotates -90, instead of 90...
    //But whatever, just inverted rotates_by_90 value. Now works just fine :)
    int rotates_by_90 = (degrees/90)%4;
    if (rotates_by_90 < 0){
        rotates_by_90 = -rotates_by_90;
    } else {
        rotates_by_90 = 4 - rotates_by_90;
    }

    struct image new_image = image_create(input_image->width, input_image->height);

    if (rotates_by_90%2 == 1){
        new_image.width = input_image->height;
        new_image.height = input_image->width;
    }

    for (uint32_t row = 0; row < input_image->height; row += 1){
        for (uint32_t index_in_row = 0; index_in_row < input_image->width; index_in_row += 1){
            uint32_t x = index_in_row;
            uint32_t y = row;
            rotate_coordinates_by_neg90(rotates_by_90, input_image->width, input_image->height, &x, &y);
            new_image.data[new_image.width * y + x] = input_image->data[input_image->width * row + index_in_row];
        }
    }

    return new_image;
}
