#include "../include/bmp_convertor.h"

#define BMP_SIGNATURE 19778
#define NO_RESERVATION 0
#define BITS_PER_PIXEL 8*sizeof(struct pixel)
#define BMP_HEADER_SIZE 40
#define AMOUNT_OF_PLANES 1
#define PELS_PER_METER 2834
#define NO_COMPRESSION 0
#define NO_CLR_USED 0

#define PADDING_MODULE 4

static int calculate_padding(const uint32_t width){
    if (width % PADDING_MODULE == 0) {return 0;}
    return PADDING_MODULE - (int) ((width * sizeof(struct pixel))%PADDING_MODULE);
}

static int read_header(FILE* file, struct bmp_header* to_header){
    return (int) fread(to_header, sizeof(struct bmp_header), 1, file);
}

enum from_bmp_status from_bmp(FILE* file, struct image* to_image){
    if (!file) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header = {0};
    if (!read_header(file, &header)){
        return READ_INVALID_HEADER;
    }
    *to_image = image_create(header.biWidth, header.biHeight);

    const int size_of_padding = calculate_padding(to_image->width);

    for (uint32_t row = 0; row < to_image->height; ++row){
        for (uint32_t index_in_row = 0; index_in_row < to_image->width; ++index_in_row){
            if (!fread(&(to_image->data[to_image->width * row + index_in_row]), sizeof(struct pixel), 1, file)){
                image_destroy(to_image);
                return READ_INVALID_BITS;
            }
        }
        if (fseek(file, size_of_padding, SEEK_CUR)){
            image_destroy(to_image);
            return READ_INVALID_PADDING;
        }
    }

    return READ_OK;
}

uint32_t calculate_image_size(const struct image* image){
    return (image->width * sizeof(struct pixel) + calculate_padding(image->width)) * image->height;
}

static struct bmp_header create_header(const struct image* image){
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = (calculate_image_size(image) + sizeof(struct bmp_header)),
            .bfReserved = NO_RESERVATION,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = (uint32_t) image->width,
            .biHeight = (uint32_t) image->height,
            .biPlanes = AMOUNT_OF_PLANES,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = NO_COMPRESSION,
            .biSizeImage = calculate_image_size(image),
            .biXPelsPerMeter = PELS_PER_METER,
            .biYPelsPerMeter = PELS_PER_METER,
            .biClrUsed = NO_CLR_USED,
            .biClrImportant = NO_CLR_USED
    };
}

enum to_bmp_status to_bmp(FILE* file, const struct image* from_image){
    struct bmp_header header = create_header(from_image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, file)){
        return WRITE_FAILED_HEADER;
    }

    if (from_image->data == NULL || from_image->width == 0 || from_image->height == 0){
        return WRITE_NO_PIXELS;
    }


    uint8_t row_padding = calculate_padding(from_image->width);

    for (uint32_t row = 0; row < from_image->height; ++row) {
        if (!fwrite(&from_image->data[row * from_image->width], from_image->width * sizeof(struct pixel), 1, file)) return WRITE_FAILED_PIXELS;
        for (int j = 0; j < row_padding; j++) {
            fputc(0, file);
        }
    }

    return WRITE_OK;
}
